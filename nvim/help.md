# HELP

## Search

`/`         -> search for word
`CTRL+n`    -> go for next result in last search
`CTRL+N`    -> go for previous result in last search
`*`         -> go for next occurence of word under the cursor
`#`         -> go for previous occurence of word under the cursor
