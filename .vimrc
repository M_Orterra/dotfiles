set nocompatible
set noswapfile
set wildmenu
set noerrorbells
set incsearch
set path+=**
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
Plug 'https://github.com/qpkorr/vim-renamer'
Plug 'tpope/vim-repeat'
Plug 'godlygeek/tabular'
Plug 'https://github.com/nvie/vim-flake8'
Plug 'mhinz/vim-startify'
Plug 'plasticboy/vim-markdown'
Plug 'tpope/vim-surround'
Plug 'https://github.com/itchyny/vim-gitbranch'
Plug 'https://github.com/itchyny/lightline.vim'
Plug 'https://github.com/qpkorr/vim-renamer'
Plug 'https://github.com/myusuf3/numbers.vim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'https://github.com/ap/vim-css-color'
Plug 'https://github.com/pangloss/vim-javascript'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'SirVer/ultisnips'
Plug 'wellle/targets.vim'
Plug 'honza/vim-snippets'
Plug 'https://github.com/morhetz/gruvbox'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'NLKNguyen/papercolor-theme'
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript','python', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markd    own', 'vue', 'yaml', 'html'] }
Plug 'preservim/nerdtree' |
            \ Plug 'Xuyuanp/nerdtree-git-plugin'
call plug#end()
filetype plugin on
"Color settings
set notermguicolors t_Co=16
"colo gruvbox
set background=dark
nnoremap <SPACE> <Nop>
let mapleader=' '
set omnifunc=syntaxcomplete#Complete
set number
set foldmethod=syntax
let g:markdown_folding = 1
let g:vim_markdown_math = 1
let g:javascript_plugin_jsdoc = 1
let g:prettier#config#use_tabs = 'true'


"lightline
set laststatus=2
"Snippets config
let g:UltiSnipsUsePythonVersion = 3
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsListSnippets="<c-tab>"
let g:UltiSnipsJumpForwardTrigger="<C-j>"
let g:UltiSnipsJumpBackwardTrigger="<C-k>"
let g:UltiSnipsRemoveSelectModeMappings = 0
"Command
"Execute python script"
nmap <F5> <Esc>:w<CR>:!clear && python3 %<CR>
"convert markdown to revealjs presentation
nmap <F7> <Esc>:w<CR>:!pandoc -t revealjs -s -o %:t:r.html % -V revealjs-url=https://revealjs.com <CR>
"map to toggle NERDTree
nmap <C-Left> :NERDTreeToggle<CR>
"Map to save with Ctrl+s
nmap <C-s> :w<CR>
"run simple http server of the folder
nmap <F8> <Esc>:w<CR>:terminal python -m SimpleHTTPServer 8080 <CR>
"save and quit
nmap <F12> <Esc>:wq<CR>
"increas/decrease splits
nmap <C-Up> :res +2<CR>
nmap <C-Down> :res -2<CR>
"Easi change window
nnoremap <Leader>h <C-w>h
nnoremap <leader>j <c-w>j
nnoremap <leader>k <c-w>k
nnoremap <leader>l <c-w>l
command CreatePDF execute "!pandoc % -o %:t:r.pdf"
nmap <C-p> :!pandoc % -o %:t:r.html -c pandoc.css && google-chrome %:t:r.html<CR>
"NERDTree
let g:NERDTreeGitStatusIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }


let g:lightline = {
      \ 'colorscheme': 'dracula', 
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }

"
"coc settings
"
nmap <silent> gd <Plug>(coc-definition)
nnoremap <silent><nowait> <space>r  :<C-u>CocCommand python.execInTerminal<cr>
