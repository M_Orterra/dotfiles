export TERM="xterm-256color"
#if [ "$TMUX" = "" ]; then tmux; fi
# Path to your oh-my-zsh installation.
export ZSH="/home/matteo/.oh-my-zsh"
# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#ZSH_THEME="spaceship"
#ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.


plugins=(
	  git
	  bundler
	  dotenv
	  #osx
	  #rake
	  #rbenv
	  #ruby
	  jsontools
	  node
	  pip
	  web-search
	  colored-man-pages
	  colorize
	  common-aliases
	  copyfile
	  virtualenv
	  zsh-autosuggestions
	  tmux
)
ZSH_TMUX_AUTOSTART_ONCE=true
source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"
export PATH=$HOME/omnetpp-5.4.1/bin:$PATH
export PATH="/Users/username/miniconda3/bin:$PATH"
export GOPATH=$HOME/go
export GEM_HOME="/usr/local/urbanopt-cli-0.6.4/gems/ruby/2.7.0"
export GEM_PATH="/usr/local/urbanopt-cli-0.6.4/gems/ruby/2.7.0"
export PATH="/usr/local/urbanopt-cli-0.6.4/ruby/bin:/usr/local/urbanopt-cli-0.6.4/gems/ruby/2.7.0/bin:/home/matteo/anaconda3/condabin:/Users/username/miniconda3/bin:/home/matteo/omnetpp-5.4.1/bin:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/home/matteo/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/local/go/bin:/home/matteo/go/bin"
export RUBYLIB="/usr/local/urbanopt-cli-0.6.4/OpenStudio/Ruby"
export RUBY_DLL_PATH="/usr/local/urbanopt-cli-0.6.4/OpenStudio/Ruby"
# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases

## Aliases
alias zshconfig="nv ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias dkr="sudo docker"
alias atlas="cd Projects/Ruritage && ssh -i WebserRuritage2019.pem ubuntu@ec2-54-76-236-190.eu-west-1.compute.amazonaws.com"
    alias iot="cd Projects/iot22_23"
alias vimconfig="cd ~/.dotfiles/nvim/ && nvim "
alias vnclab="ssh -L 5901:127.0.0.1:5901 130.192.163.39"
alias cat="batcat"
alias dotfiles="~/.dotfiles & nvim" 
#alias nv="~/Programs/nvim.appimage"
alias nv="nvim"
alias saveVault="cd Documents/ObsidianVault && ga . && gc -m 'updates'&& gp"
# End of Aliases

#virtualenv
VIRTUAL_ENV_DISABLE_PROMPT=1

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/matteo/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/matteo/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/matteo/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/matteo/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

alias config='/usr/bin/git --git-dir=/home/matteo/.cfg/ --work-tree=/home/matteo'
#autoload -Uz compinit
#compinit
# Completion for kitty
#kitty + complete setup zsh | source /dev/stdin

## Spaceship configuration
#SPACESHIP_PROMPT_ORDER=(
#  time     #
#  vi_mode  # these sections will be
#  user     # before prompt char
#  host     #
#  char
#  dir
#  git
#  node
#  golang
#  docker
#  #venv
#  #pyenv
#  conda
#)
#SPACESHIP_TIME_SHOW=true
##

export NVM_DIR="/home/matteo/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

eval "$(starship init zsh)"
export MANPATH=$HOME/tools/ripgrep/doc/man:$MANPATH
export FPATH=$HOME/tools/ripgrep/complete:$FPATH


export FLYCTL_INSTALL="/home/matteo/.fly"
export PATH="$FLYCTL_INSTALL/bin:$PATH"
